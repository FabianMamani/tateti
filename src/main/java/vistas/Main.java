package vistas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Main {

	private JFrame frmTatetiproject;
	private JButton btnIniciar;
	private JComboBox nPlayers;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frmTatetiproject.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmTatetiproject = new JFrame();
		frmTatetiproject.setResizable(false);
		frmTatetiproject.setTitle("TA-TE-TI");
		frmTatetiproject.setBounds(100, 100, 308, 262);
		frmTatetiproject.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmTatetiproject.getContentPane().setLayout(null);
		
		JLabel lblTateti = new JLabel("TA-TE-TI");
		lblTateti.setVerticalAlignment(SwingConstants.BOTTOM);
		lblTateti.setFont(new Font("Yu Gothic Medium", Font.BOLD, 20));
		lblTateti.setBounds(77, 24, 111, 59);
		frmTatetiproject.getContentPane().add(lblTateti);
		
		JLabel players = new JLabel("Nro de Jugadores:");
		players.setFont(new Font("Yu Gothic Light", Font.BOLD, 14));
		players.setBounds(35, 120, 132, 23);
		frmTatetiproject.getContentPane().add(players);
		
		nPlayers = new JComboBox();
		nPlayers.setModel(new DefaultComboBoxModel(new String[] {"", "1", "2"}));
		nPlayers.setBounds(177, 122, 64, 20);
		frmTatetiproject.getContentPane().add(nPlayers);
		
		btnIniciar = new JButton("Iniciar");
		click(btnIniciar);
		btnIniciar.setFont(new Font("Yu Gothic Light", Font.BOLD, 11));
		btnIniciar.setBounds(77, 170, 95, 23);
		frmTatetiproject.getContentPane().add(btnIniciar);
	}
	

	private int cantidadPlayers() {
		 return (nPlayers.getSelectedIndex()); 
	}
	private void click(JButton btn) {
		btn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (cantidadPlayers() > 0) {
					System.out.println("Iniciar");
				}else {
					JOptionPane.showMessageDialog(null, "Ingrese cantidad de Jugadores");
				}
			}
		});
	}
}
