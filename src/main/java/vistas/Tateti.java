package vistas;

import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import javax.swing.UIManager;
import javax.swing.ListSelectionModel;

public class Tateti {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tateti window = new Tateti();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tateti() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
//		frame.setBounds(100, 100, 450, 300);
		frame.setBounds(100, 100, 450, 399);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		JTable Tablero = new JTable();
		Tablero.setEnabled(false);
		Tablero.setRowSelectionAllowed(false);
		Tablero.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		Tablero.setBorder(UIManager.getBorder("DesktopIcon.border"));
		Tablero.setModel(new DefaultTableModel(
			new Object[][] {
				{"1", new JButton("Clic aquí"), "3"},
				{"4", "5", "6"},
				{"7", "8", "9"},
			},
			new String[] {
				"columna1", "columna2", "columna3"
			}
		));
		Tablero.setRowHeight(100);
		
		Tablero.getColumnModel().getColumn(0).setPreferredWidth(100);
		Tablero.getColumnModel().getColumn(1).setPreferredWidth(100);
		Tablero.getColumnModel().getColumn(2).setPreferredWidth(100);
		 
		Tablero.setBounds(56, 30, 300, 300);
		frame.getContentPane().add(Tablero);
	}
}
